<?php

/**
 * @file
 * Contains \Drupal\css_tools\CssProcessor.
 */

namespace Drupal\css_tools;

/**
 * @todo.
 */
class CssProcessor {

  /**
   * Filter a chunk of CSS text.
   *
   * This function disassembles the CSS into a raw format that makes it easier
   * for our tool to work, then runs it through the filter and reassembles it.
   * If you find that you want the raw data for some reason or another, you
   * can use the disassemble/assemble functions yourself.
   *
   * @param $css
   *   The CSS text to filter.
   * @param $compressed
   *   If true, generate compressed output; if false, generate pretty output.
   *   Defaults to TRUE.
   *
   * @return string
   */
  public function filter($css, $compressed = TRUE) {
    $css_data = $this->disassemble($css);

    // Note: By using this function yourself you can control the allowed
    // properties and values list.
    $filtered = $this->filterCssData($css_data);

    return $compressed ? $this->compress($filtered) : $this->assemble($filtered);
  }

  /**
   * Re-assemble a css string and format it nicely.
   *
   * @param array $css_data
   *   An array of css data, as produced by @see self::css_disassemble()
   *   disassembler and the @see self::filterCssData() filter.
   *
   * @return string $css
   *   css optimized for human viewing.
   */
  public function assemble($css_data) {
    // Initialize the output.
    $css = '';
    // Iterate through all the statements.
    foreach ($css_data as $selector_str => $declaration) {
      // Add the selectors, separating them with commas and line feeds.
      $css .= strpos($selector_str, ',') === FALSE ? $selector_str : str_replace(", ", ",\n", $selector_str);
      // Add the opening curly brace.
      $css .= " {\n";
      // Iterate through all the declarations.
      foreach ($declaration as $property => $value) {
        $css .= "  " . $property . ": " . $value . ";\n";
      }
      // Add the closing curly brace.
      $css .= "}\n\n";
    }
    // Return the output.
    return $css;
  }

  /**
   * Compress css data (filter it first!) to optimize for use on view.
   *
   * @param array $css_data
   *   An array of css data, as produced by @see self::disassemble()
   *   disassembler and the @see self::filterCssData() filter.
   *
   * @return string $css
   *   css optimized for use.
   */
  public function compress($css_data) {
    // Initialize the output.
    $css = '';
    // Iterate through all the statements.
    foreach ($css_data as $selector_str => $declaration) {
      if (empty($declaration)) {
        // Skip this statement if filtering removed all parts of the declaration.
        continue;
      }
      // Add the selectors, separating them with commas.
      $css .= $selector_str;
      // And, the opening curly brace.
      $css .= "{";
      // Iterate through all the statement properties.
      foreach ($declaration as $property => $value) {
        $css .= $property . ':' . $value . ';';
      }
      // Add the closing curly brace.
      $css .= "}";
    }
    // Return the output.
    return $css;
  }

  /**
   * Disassemble the css string.
   *
   * Strip the css of irrelevant characters, invalid/malformed selectors and
   * declarations, and otherwise prepare it for processing.
   *
   * @param string $css
   *   A string containing the css to be disassembled.
   *
   * @return array $disassembled_css
   *   An array of disassembled, slightly cleaned-up/formatted css statements.
   */
  public function disassemble($css) {
    $disassembled_css = array();
    // Remove comments.
    $css = preg_replace("/\/\*(.*)?\*\//Usi", "", $css);
    // Split out each statement. Match either a right curly brace or a semi-colon
    // that precedes a left curly brace with no right curly brace separating them.
    $statements = preg_split('/}|;(?=[^}]*{)/', $css);

    // If we have any statements, parse them.
    if (!empty($statements)) {
      // Iterate through all of the statements.
      foreach ($statements as $statement) {
        // Get the selector(s) and declaration.
        if (empty($statement) || !strpos($statement, '{')) {
          continue;
        }

        list($selector_str, $declaration) = explode('{', $statement);

        // If the selector exists, then disassemble it, check it, and regenerate
        // the selector string.
        $selector_str = empty($selector_str) ? FALSE : $this->disassembleSelector($selector_str);
        if (empty($selector_str)) {
          // No valid selectors. Bomb out and start the next item.
          continue;
        }

        // Disassemble the declaration, check it and tuck it into an array.
        if (!isset($disassembled_css[$selector_str])) {
          $disassembled_css[$selector_str] = array();
        }
        $disassembled_css[$selector_str] += $this->disassembleDeclaration($declaration);
      }
    }
    return $disassembled_css;
  }

  /**
   * @todo.
   */
  protected function disassembleSelector($selector_str) {
    // Get all selectors individually.
    $selectors = explode(",", trim($selector_str));
    // Iterate through all the selectors, sanity check them and return if they
    // pass. Note that this handles 0, 1, or more valid selectors gracefully.
    foreach ($selectors as $key => $selector) {
      // Replace un-needed characters and do a little cleanup.
      $selector = preg_replace("/[\n|\t|\\|\s]+/", ' ', trim($selector));
      // Make sure this is still a real selector after cleanup.
      if (!empty($selector)) {
        $selectors[$key] = $selector;
      }
      else {
        // Selector is no good, so we scrap it.
        unset($selectors[$key]);
      }
    }
    // Check for malformed selectors; if found, we skip this declaration.
    if (empty($selectors)) {
      return FALSE;
    }
    return implode(', ', $selectors);
  }

  /**
   * @todo.
   */
  protected function disassembleDeclaration($declaration) {
    $formatted_statement = array();
    $propval_pairs = explode(";", $declaration);
    // Make sure we actually have some properties to work with.
    if (!empty($propval_pairs)) {
      // Iterate through the remains and parse them.
      foreach ($propval_pairs as $propval_pair) {
        // Check that we have a ':', otherwise it's an invalid pair.
        if (strpos($propval_pair, ':') === FALSE) {
          continue;
        }
        // Clean up the current property-value pair.
        $propval_pair = preg_replace("/[\n|\t|\\|\s]+/", ' ', trim($propval_pair));
        // Explode the remaining fragments some more, but clean them up first.
        list($property, $value) = explode(':', $propval_pair, 2);
        // If the property survived, toss it onto the stack.
        if (!empty($property)) {
          $formatted_statement[trim($property)] = trim($value);
        }
      }
    }
    return $formatted_statement;
  }

  /**
   * Run disassembled $css through the filter.
   *
   * @param $css
   *   CSS code disassembled by self::disassemble().
   * @param array $allowed_properties
   *   A list of properties that are allowed by the filter. If empty
   *   filterDefaultAllowedProperties() will provide the
   *   list.
   * @param array $allowed_values
   *   A list of values that are allowed by the filter. If empty
   *   filterDefaultAllowedValues() will provide the
   *   list.
   * @param string $allowed_values_regex
   * @param string $disallowed_values_regex
   *
   * @return array
   *   An array of disassembled, filtered CSS.
   */
  public function filterCssData($css, $allowed_properties = array(), $allowed_values = array(), $allowed_values_regex = '', $disallowed_values_regex = '') {
    // Retrieve the default list of allowed properties if none is provided.
    $allowed_properties = $allowed_properties ?: $this->filterDefaultAllowedProperties();
    // Retrieve the default list of allowed values if none is provided.
    $allowed_values = $allowed_values ?: $this->filterDefaultAllowedValues();
    // Define allowed values regex if none is provided.
    $allowed_values_regex = $allowed_values_regex ?: '/(#[0-9a-f]+|rgb\(\d+%?,\d*%?,?\d*%?\)?|\d{0,2}\.?\d{0,2}(cm|em|ex|in|mm|pc|pt|px|%|,|\))?)/';
    // Define disallowed url() value contents, if none is provided.
    // $disallowed_values_regex = $disallowed_values_regex ?: '/[url|expression]\s*\(\s*[^\s)]+?\s*\)\s*/';
    $disallowed_values_regex = $disallowed_values_regex ?: '/(url|expression)/';

    foreach ($css as $selector_str => $declaration) {
      foreach ($declaration as $property => $value) {
        if (!in_array($property, $allowed_properties)) {
          // $filtered['properties'][$selector_str][$property] = $value;
          unset($css[$selector_str][$property]);
          continue;
        }
        $value = str_replace('!important', '', $value);
        if (preg_match($disallowed_values_regex, $value) || !(in_array($value, $allowed_values) || preg_match($allowed_values_regex, $value))) {
          // $filtered['values'][$selector_str][$property] = $value;
          unset($css[$selector_str][$property]);
          continue;
        }
      }
    }
    return $css;
  }

  /**
   * Provide a default list of allowed properties by the filter.
   *
   * @codeCoverageIgnore
   */
  protected function filterDefaultAllowedProperties() {
    return [
      'azimuth',
      'background',
      'background-color',
      'background-image',
      'background-repeat',
      'background-attachment',
      'background-position',
      'border',
      'border-top-width',
      'border-right-width',
      'border-bottom-width',
      'border-left-width',
      'border-width',
      'border-top-color',
      'border-right-color',
      'border-bottom-color',
      'border-left-color',
      'border-color',
      'border-top-style',
      'border-right-style',
      'border-bottom-style',
      'border-left-style',
      'border-style',
      'border-top',
      'border-right',
      'border-bottom',
      'border-left',
      'clear',
      'color',
      'cursor',
      'direction',
      'display',
      'elevation',
      'float',
      'font',
      'font-family',
      'font-size',
      'font-style',
      'font-variant',
      'font-weight',
      'height',
      'letter-spacing',
      'line-height',
      'margin',
      'margin-top',
      'margin-right',
      'margin-bottom',
      'margin-left',
      'overflow',
      'padding',
      'padding-top',
      'padding-right',
      'padding-bottom',
      'padding-left',
      'pause',
      'pause-after',
      'pause-before',
      'pitch',
      'pitch-range',
      'richness',
      'speak',
      'speak-header',
      'speak-numeral',
      'speak-punctuation',
      'speech-rate',
      'stress',
      'text-align',
      'text-decoration',
      'text-indent',
      'text-transform',
      'unicode-bidi',
      'vertical-align',
      'voice-family',
      'volume',
      'white-space',
      'width',
      'fill',
      'fill-opacity',
      'fill-rule',
      'stroke',
      'stroke-width',
      'stroke-linecap',
      'stroke-linejoin',
      'stroke-opacity',
    ];
  }

  /**
   * Provide a default list of allowed values by the filter.
   *
   * @codeCoverageIgnore
   */
  protected function filterDefaultAllowedValues() {
    return [
      'auto',
      'aqua',
      'black',
      'block',
      'blue',
      'bold',
      'both',
      'bottom',
      'brown',
      'capitalize',
      'center',
      'collapse',
      'dashed',
      'dotted',
      'fuchsia',
      'gray',
      'green',
      'italic',
      'inherit',
      'left',
      'lime',
      'lowercase',
      'maroon',
      'medium',
      'navy',
      'normal',
      'nowrap',
      'olive',
      'pointer',
      'purple',
      'red',
      'right',
      'solid',
      'silver',
      'teal',
      'top',
      'transparent',
      'underline',
      'uppercase',
      'white',
      'yellow',
    ];
  }

}
