<?php

/**
 * @file
 * Contains \Drupal\css_tools\CssStorage.
 */

namespace Drupal\css_tools;

use Drupal\Core\KeyValueStore\KeyValueExpirableFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Provides CSS filtering. Contains a disassembler, filter, compressor, and
 * decompressor.
 *
 * The general usage of this tool is:
 *
 * To simply filter CSS:
 * @code
 *   $filtered_css = \Drupal\css_tools\CssTools::storage()->filter($css, TRUE);
 * @endcode
 *
 * In the above, if the second argument is TRUE, the returned CSS will
 * be compressed. Otherwise it will be returned in a well formatted
 * syntax.
 *
 * To cache unfiltered CSS in a file, which will be filtered:
 *
 * @code
 *   $filename = \Drupal\css_tools\CssTools::storage()->cache($css, TRUE);
 * @endcode
 *
 * In the above, if the second argument is FALSE, the CSS will not be filtered.
 *
 * This file will be cached within the Drupal files system. This system cannot
 * detect when this file changes, so it is YOUR responsibility to remove and
 * re-cache this file when the CSS is changed. Your system should also contain
 * a backup method of re-generating the CSS cache in case it is removed, so
 * that it is easy to force a re-cache by simply deleting the contents of the
 * directory.
 *
 * Finally, if for some reason your application cannot store the filename
 * you can use a unique ID:
 *
 * @code
 *   $filename = \Drupal\css_tools\CssTools::storage()->store($id, $css, TRUE);
 * @endcode
 *
 * Then later on:
 * @code
 *   $filename = \Drupal\css_tools\CssTools::storage()->retrieve($id);
 *   drupal_add_css($filename);
 * @endcode
 *
 * The CSS that was generated will be stored in the database, so even if the
 * file was removed the cached CSS will be used. If the CSS cache is
 * cleared you may be required to regenerate your CSS. This will normally
 * only be cleared by an administrator operation, not during normal usage.
 *
 * You may remove your stored CSS this way:
 *
 * @code
 *   \Drupal\css_tools\CssTools::storage()->clear($id);
 * @endcode
 */
class CssStorage {

  use StringTranslationTrait;

  protected $cssProcessor;

  /**
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreExpirableInterface
   */
  protected $keyValue;

  /**
   * @param \Drupal\css_tools\CssProcessor $css_processor
   * @param \Drupal\Core\KeyValueStore\KeyValueExpirableFactoryInterface $key_value_factory
   */
  public function __construct(CssProcessor $css_processor, KeyValueExpirableFactoryInterface $key_value_factory) {
    $this->cssProcessor = $css_processor;
    $this->keyValue = $key_value_factory->get('css_tools');
  }

  /**
   * @todo.
   */
  public function filter($css, $compressed = TRUE) {
    return $this->cssProcessor->filter($css, $compressed);
  }

  /**
   * Store CSS with a given id and return the filename to use.
   *
   * This function associates a piece of CSS with an id, and stores the
   * cached filename and the actual CSS for later use with
   * self::retrieve.
   */
  public function store($id, $css, $filter = TRUE) {
    $data = $this->keyValue->get($id);
    if ($data) {
      if (isset($data['filename']) && file_exists($data['filename'])) {
        $this->fileUnmanagedDelete($data['filename']);
      }
      // Remove any previous records.
      $this->keyValue->delete($id);
    }

    $filename = $this->cache($css, $filter);

    $this->keyValue->set($id, [
      'filename' => $filename,
      'css' => $css,
      'filter' => intval($filter),
    ]);

    return $filename;
  }

  /**
   * Retrieve a filename associated with an id of previously cached CSS.
   *
   * This will ensure the file still exists and, if not, create it.
   */
  public function retrieve($id) {
    if (!$data = $this->keyValue->get($id)) {
      return;
    }

    if (!file_exists($data['filename'])) {
      $filename = $this->cache($data['css'], $data['filter']);
      if ($filename != $data['filename']) {
        $data['filename'] = $filename;
        $this->keyValue->set($id, $data);
      }
    }

    return $data['filename'];
  }

  /**
   * Remove stored CSS and any associated file.
   */
  public function clear($id) {
    if (!$data = $this->keyValue->get($id)) {
      return;
    }

    if (file_exists($data['filename'])) {
      $this->fileUnmanagedDelete($data['filename']);
      // If we remove an existing file, there may be cached pages that refer
      // to it. We must get rid of them: FIXME same format in D7?
      // cache_clear_all();
    }

    $this->keyValue->delete($id);
  }

  /**
   * Write a chunk of CSS to a temporary cache file and return the file name.
   *
   * This function optionally filters the CSS (always compressed, if so) and
   * generates a unique filename based upon md5. It returns that filename that
   * can be used with drupal_add_css(). Note that as a cache file, technically
   * this file is volatile so it should be checked before it is used to ensure
   * that it exists.
   *
   * You can use file_exists() to test for the file and file_delete() to remove
   * it if it needs to be cleared.
   *
   * @param $css
   *   A chunk of well-formed CSS text to cache.
   * @param $filter
   *   If TRUE the css will be filtered. If FALSE the text will be cached
   *   as-is.
   *
   * @return string|null
   *   The filename the CSS will be cached in.
   */
  public function cache($css, $filter = TRUE) {
    if ($filter) {
      $css = $this->filter($css);
    }

    // Create the css/ within the files folder.
    $path = 'public://css_tools/css';
    if (!$this->filePrepareDirectory($path, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS)) {
      $this->drupalSetMessage($this->t('Unable to create CSS Tools cache directory. Check the permissions on your files directory.'), 'error');
      return;
    }

    $filename = $path . '/' . $this->hashData($css) . '.css';

    // This will do renames if the file already exists, ensuring we don't
    // accidentally overwrite other files who share the same md5. Yes this
    // is a very miniscule chance but it's safe.
    $filename = $this->fileUnmanagedSaveData($css, $filename);

    return $filename;
  }

  /**
   * @todo.
   */
  protected function hashData($data) {
    // @todo Is this slow? Does it matter if it is?
    return md5($data);
  }

  /**
   * Wraps file_unmanaged_delete().
   */
  protected function fileUnmanagedDelete($path) {
    return file_unmanaged_delete($path);
  }
  /**
   * Wraps file_prepare_directory().
   */
  protected function filePrepareDirectory(&$directory, $options = FILE_MODIFY_PERMISSIONS) {
    return file_prepare_directory($directory, $options);
  }

  /**
   * Wraps file_unmanaged_save_data().
   */
  protected function fileUnmanagedSaveData($data, $destination = NULL, $replace = FILE_EXISTS_RENAME) {
    return file_unmanaged_save_data($data, $destination, $replace);
  }

  /**
   * Wraps drupal_set_message().
   */
  protected function drupalSetMessage($message = NULL, $type = 'status', $repeat = FALSE) {
    return drupal_set_message($message, $type, $repeat);
  }

}
