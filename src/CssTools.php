<?php

/**
 * @file
 * Contains \Drupal\css_tools\CssTools.
 */

namespace Drupal\css_tools;

/**
 * Static service container wrapper for CSS Tools.
 */
class CssTools {

  /**
   * @return \Drupal\css_tools\CssStorage
   */
  public static function storage() {
    return \Drupal::service('css_tools.storage');
  }

  /**
   * @return \Drupal\css_tools\CssProcessor
   */
  public static function processor() {
    return \Drupal::service('css_tools.storage');
  }

}
