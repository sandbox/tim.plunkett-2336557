<?php

/**
 * @file
 * Contains \Drupal\Tests\css_tools\Unit\CssProcessorTest.
 */

namespace Drupal\Tests\css_tools\Unit;

use Drupal\css_tools\CssProcessor;
use Drupal\Tests\UnitTestCase;

/**
 * @coversDefaultClass \Drupal\css_tools\CssProcessor
 *
 * @group css_tools
 */
class CssProcessorTest extends UnitTestCase {

  /**
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreExpirableInterface|\PHPUnit_Framework_MockObject_MockObject
   */
  protected $keyValue;

  /**
   * @var \Drupal\css_tools\CssProcessor
   */
  protected $cssProcessor;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->cssProcessor = new CssProcessor();
  }

  /**
   * @covers ::filter
   *
   * @dataProvider providerTestFilter
   */
  public function testFilter($css, $expected, $compressed = TRUE) {
    $this->assertSame($expected, $this->cssProcessor->filter($css, $compressed));
  }

  public function providerTestFilter() {
    $data = [];
    $data[] = [
      ')',
      '',
    ];
    $data[] = [
      'p',
      '',
    ];
    $data[] = [
      'p {color}',
      '',
    ];
    $data[] = [
      ' {color: red; }',
      '',
    ];
    $data[] = [
      'p {color: red; }',
      'p{color:red;}',
    ];
    $data[] = [
      ';p {color: red; font-size: 12px;}',
      'p{color:red;font-size:12px;}',
    ];
    $data[] = [
      'h1,p {color: red; font-size: 12px;}',
      'h1, p{color:red;font-size:12px;}',
    ];
    $data[] = [
      '@import url("other.css");p {color: red;}',
      'p{color:red;}',
    ];
    $data[] = [
      "#some-id {\n  background-image: url(http://example.com/example.gif);\n  background-color: red;\n}",
      '#some-id{background-color:red;}',
    ];

    $data[] = [
      'p {color: red; }',
      "p {\n  color: red;\n}\n\n",
      FALSE,
    ];
    $data[] = [
      ';p {color: red; font-size: 12px;}',
      "p {\n  color: red;\n  font-size: 12px;\n}\n\n",
      FALSE,
    ];
    $data[] = [
      'h1,p {color: red; font-size: 12px;}',
      "h1,\np {\n  color: red;\n  font-size: 12px;\n}\n\n",
      FALSE,
    ];
    $data[] = [
      '@import url("other.css");p {color: red;}',
      "p {\n  color: red;\n}\n\n",
      FALSE,
    ];
    $data[] = [
      "#some-id {\n  background-image: url(http://example.com/example.gif);\n  background-color: red;\n}",
      "#some-id {\n  background-color: red;\n}\n\n",
      FALSE,
    ];
    return $data;
  }

  /**
   * @covers ::assemble
   *
   * @dataProvider providerTestAssemble
   */
  public function testAssemble($css, $expected) {
    $this->assertSame($expected, $this->cssProcessor->assemble($css));
  }

  public function providerTestAssemble() {
    $data = [];
    $data[] = [
      ['p' => []],
      "p {\n}\n\n",
    ];
    $data[] = [
      ['p' => ['color' => 'red']],
      "p {\n  color: red;\n}\n\n",
    ];
    $data[] = [
      ['p' => [
        'color' => 'red',
        'font-size' => '12px',
      ]],
      "p {\n  color: red;\n  font-size: 12px;\n}\n\n",
    ];
    $data[] = [
      ['h1, p' => [
        'color' => 'red',
        'font-size' => '12px',
      ]],
      "h1,\np {\n  color: red;\n  font-size: 12px;\n}\n\n",
    ];
    $data[] = [
      ['#some-id' => ['background-image' => 'url(http://example.com/example.gif)']],
      "#some-id {\n  background-image: url(http://example.com/example.gif);\n}\n\n",
    ];
    $data[] = [
      [
        'p' => [
          'color' => 'red',
          'font-size' => '12px',
        ],
        'h1' => [
          'color' => 'blue',
        ],
      ],
      "p {\n  color: red;\n  font-size: 12px;\n}\n\nh1 {\n  color: blue;\n}\n\n",
    ];
    return $data;
  }

  /**
   * @covers ::filterCssData
   *
   * @dataProvider providerTestFilterCssData
   */
  public function testFilterCssData($css, $expected = NULL) {
    if (!$expected) {
      $expected = $css;
    }
    $this->assertSame($expected, $this->cssProcessor->filterCssData($css));
  }

  public function providerTestFilterCssData() {
    $data = [];
    $data[] = [
      ['p' => []],
    ];
    $data[] = [
      ['p' => ['color' => 'red']],
    ];
    $data[] = [
      ['p' => [
        'color' => 'red',
        'font-size' => '12px',
      ]],
    ];
    $data[] = [
      ['p' => [
        'illegal' => 'nope',
        'font-size' => '12px',
      ]],
      ['p' => [
        'font-size' => '12px',
      ]],
    ];
    $data[] = [
      ['#some-id' => [
        'background-image' => 'url(http://example.com/example.gif)',
        'background-color' => 'red',
      ]],
      ['#some-id' => ['background-color' => 'red']],
    ];
    return $data;
    $data[] = [
      [
        'p' => [
          'color' => 'red',
          'font-size' => '12px',
        ],
        'h1' => [
          'color' => 'blue',
        ],
      ],
      "p {\n  color: red;\n  font-size: 12px;\n}\n\nh1 {\n  color: blue;\n}\n\n",
    ];
    return $data;
  }

  /**
   * @covers ::compress
   *
   * @dataProvider providerTestCompress
   */
  public function testCompress($css, $expected) {
    $this->assertSame($expected, $this->cssProcessor->compress($css));
  }

  public function providerTestCompress() {
    $data = [];
    $data[] = [
      ['p' => []],
      '',
    ];
    $data[] = [
      ['p' => ['color' => 'red']],
      'p{color:red;}',
    ];
    $data[] = [
      ['p' => [
        'color' => 'red',
        'font-size' => '12px',
      ]],
      'p{color:red;font-size:12px;}',
    ];
    $data[] = [
      ['h1, p' => [
        'color' => 'red',
        'font-size' => '12px',
      ]],
      'h1, p{color:red;font-size:12px;}',
    ];
    $data[] = [
      ['#some-id' => ['background-image' => 'url(http://example.com/example.gif)']],
      '#some-id{background-image:url(http://example.com/example.gif);}',
    ];
    $data[] = [
      [
        'p' => [
          'color' => 'red',
          'font-size' => '12px',
        ],
        'h1' => [
          'color' => 'blue',
        ],
      ],
      "p{color:red;font-size:12px;}h1{color:blue;}",
    ];
    return $data;
  }

  /**
   * @covers ::disassemble
   *
   * @dataProvider providerTestDisassemble
   */
  public function testDisassemble($css, $expected) {
    $this->assertSame($expected, $this->cssProcessor->disassemble($css));
  }

  public function providerTestDisassemble() {
    $data = [];
    $data[] = [
      ')',
      [],
    ];
    $data[] = [
      'p',
      [],
    ];
    $data[] = [
      'p {color}',
      ['p' => []],
    ];
    $data[] = [
      ' {color: red; }',
      [],
    ];
    $data[] = [
      'p {color: red; }',
      ['p' => ['color' => 'red']],
    ];
    $data[] = [
      ';p {color: red; font-size: 12px;}',
      ['p' => [
        'color' => 'red',
        'font-size' => '12px',
      ]],
    ];
    $data[] = [
      'h1,p {color: red; font-size: 12px;}',
      ['h1, p' => [
        'color' => 'red',
        'font-size' => '12px',
      ]],
    ];
    $data[] = [
      '@import url("other.css");p {color: red;}',
      ['p' => ['color' => 'red']],
    ];
    $data[] = [
      "#some-id {\n  background-image: url(http://example.com/example.gif);\n}",
      ['#some-id' => ['background-image' => 'url(http://example.com/example.gif)']],
    ];
    return $data;
  }

  /**
   * @covers ::disassembleDeclaration
   *
   * @dataProvider providerTestDisassembleDeclaration
   */
  public function testDisassembleDeclaration($css, $expected) {
    $this->assertSame($expected, $this->cssProcessor->disassemble($css));
  }

  public function providerTestDisassembleDeclaration() {
    $data = [];
    $data[] = [
      'p {}',
      ['p' => []],
    ];
    $data[] = [
      'p {color}',
      ['p' => []],
    ];
    $data[] = [
      'p {:red}',
      ['p' => []],
    ];
    $data[] = [
      'p {color: red; }',
      ['p' => ['color' => 'red']],
    ];
    $data[] = [
      'p {color: red}',
      ['p' => ['color' => 'red']],
    ];
    $data[] = [
      'p {color: red; font-size: 12px;}',
      ['p' => [
        'color' => 'red',
        'font-size' => '12px',
      ]],
    ];
    $data[] = [
      'p {: red; font-size: 12px;}',
      ['p' => [
        'font-size' => '12px',
      ]],
    ];
    $data[] = [
      "p {\n  background-image: url(http://example.com/example.gif);\n}",
      ['p' => ['background-image' => 'url(http://example.com/example.gif)']],
    ];
    return $data;
  }

  /**
   * @covers ::disassembleSelector
   *
   * @dataProvider providerTestDisassembleSelector
   */
  public function testDisassembleSelector($css, $expected) {
    $this->assertSame($expected, $this->cssProcessor->disassemble($css));
  }

  public function providerTestDisassembleSelector() {
    $data = [];
    $data[] = [
      'p {color: red; }',
      ['p' => ['color' => 'red']],
    ];
    $data[] = [
      'h1,p {color: red;}',
      ['h1, p' => [
        'color' => 'red',
      ]],
    ];
    $data[] = [
      "h1\n,\tp {color: red;}",
      ['h1, p' => [
        'color' => 'red',
      ]],
    ];
    $data[] = [
      "h1\n, p, {color: red;}",
      ['h1, p' => [
        'color' => 'red',
      ]],
    ];
    $data[] = [
      ", {color: red;}",
      [],
    ];
    return $data;
  }

}
