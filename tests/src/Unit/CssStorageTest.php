<?php

/**
 * @file
 * Contains \Drupal\Tests\css_tools\Unit\CssStorageTest.
 */

namespace Drupal\Tests\css_tools\Unit;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Tests\UnitTestCase;
use org\bovigo\vfs\vfsStream;

/**
 * @coversDefaultClass \Drupal\css_tools\CssStorage
 *
 * @group css_tools
 */
class CssStorageTest extends UnitTestCase {

  /**
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreExpirableInterface|\PHPUnit_Framework_MockObject_MockObject
   */
  protected $keyValue;

  /**
   * @var \Drupal\css_tools\CssProcessor|\PHPUnit_Framework_MockObject_MockObject
   */
  protected $cssProcessor;

  /**
   * @var \Drupal\css_tools\CssStorage|\PHPUnit_Framework_MockObject_MockObject
   */
  protected $cssStorage;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $container = new ContainerBuilder();
    $container->set('string_translation', $this->getStringTranslationStub());
    \Drupal::setContainer($container);

    $this->keyValue = $this->getMock('Drupal\Core\KeyValueStore\KeyValueStoreExpirableInterface');
    $key_value_factory = $this->getMock('Drupal\Core\KeyValueStore\KeyValueExpirableFactoryInterface');
    $key_value_factory->expects($this->atLeastOnce())
      ->method('get')
      ->with('css_tools')
      ->willReturn($this->keyValue);
    $this->cssProcessor = $this->getMock('Drupal\css_tools\CssProcessor');
    $this->cssStorage = $this->getMockBuilder('Drupal\css_tools\CssStorage')
      ->setConstructorArgs([$this->cssProcessor, $key_value_factory])
      ->setMethods(['fileUnmanagedDelete', 'filePrepareDirectory', 'fileUnmanagedSaveData', 'drupalSetMessage', 'hashData'])
      ->getMock();
  }

  /**
   * @covers ::store
   */
  public function testStore() {
    $hash = 'qwer';
    $id = 'adf';
    $css = 'p{color:red;}';
    $filter = TRUE;
    $expected_filename = "public://css_tools/css/$hash.css";

    $this->cssStorage->expects($this->once())
      ->method('filePrepareDirectory')
      ->willReturn(TRUE);
    $this->cssStorage->expects($this->once())
      ->method('fileUnmanagedSaveData')
      ->with($css, $expected_filename)
      ->willReturnArgument(1);
    $this->cssStorage->expects($this->once())
      ->method('hashData')
      ->with($css)
      ->willReturn($hash);

    $this->keyValue->expects($this->once())
      ->method('set')
      ->with($id, [
        'filename' => $expected_filename,
        'css' => $css,
        'filter' => TRUE,
      ]);

    $this->cssProcessor->expects($this->once())
      ->method('filter')
      ->with($css)
      ->willReturn($css);

    $filename = $this->cssStorage->store($id, $css, $filter);
    $this->assertSame($expected_filename, $filename);
  }

  /**
   * @covers ::store
   */
  public function testStoreOverwrite() {
    vfsStream::setup('css_tools');

    $hash = 'qwer';
    $id = 'adf';
    $css = 'p{color:red;}';
    $filter = TRUE;
    $expected_filename = "public://css_tools/css/$hash.css";

    vfsStream::create(['css' => ["$hash.css" => $css]]);
    $vfs_filename = vfsStream::url("css_tools/css/$hash.css");

    $this->cssStorage->expects($this->once())
      ->method('filePrepareDirectory')
      ->willReturn(TRUE);
    $this->cssStorage->expects($this->once())
      ->method('fileUnmanagedDelete')
      ->with($vfs_filename);
    $this->cssStorage->expects($this->once())
      ->method('fileUnmanagedSaveData')
      ->with($css, $expected_filename)
      ->willReturnArgument(1);
    $this->cssStorage->expects($this->once())
      ->method('hashData')
      ->with($css)
      ->willReturn($hash);
    $this->keyValue->expects($this->once())
      ->method('get')
      ->willReturn([
        'filename' => $vfs_filename,
        'css' => $css,
        'filter' => TRUE,
      ]);
    $this->keyValue->expects($this->once())
      ->method('delete')
      ->with($id);
    $this->keyValue->expects($this->once())
      ->method('set')
      ->with($id, [
        'filename' => $expected_filename,
        'css' => $css,
        'filter' => TRUE,
      ]);

    $this->cssProcessor->expects($this->once())
      ->method('filter')
      ->with($css)
      ->willReturn($css);

    $filename = $this->cssStorage->store($id, $css, $filter);
    $this->assertSame($expected_filename, $filename);
  }

  /**
   * @covers ::store
   */
  public function testStoreExistingButMissing() {
    vfsStream::setup('css_tools');

    $hash = 'qwer';
    $id = 'adf';
    $css = 'p{color:red;}';
    $filter = TRUE;
    $expected_filename = "public://css_tools/css/$hash.css";

    $this->cssStorage->expects($this->once())
      ->method('filePrepareDirectory')
      ->willReturn(TRUE);
    $this->cssStorage->expects($this->never())
      ->method('fileUnmanagedDelete');
    $this->cssStorage->expects($this->once())
      ->method('fileUnmanagedSaveData')
      ->with($css, $expected_filename)
      ->willReturnArgument(1);
    $this->cssStorage->expects($this->once())
      ->method('hashData')
      ->with($css)
      ->willReturn($hash);
    $this->keyValue->expects($this->once())
      ->method('get')
      ->willReturn([
        'filename' => 'invalid',
        'css' => $css,
        'filter' => TRUE,
      ]);
    $this->keyValue->expects($this->once())
      ->method('delete')
      ->with($id);
    $this->keyValue->expects($this->once())
      ->method('set')
      ->with($id, [
        'filename' => $expected_filename,
        'css' => $css,
        'filter' => TRUE,
      ]);

    $this->cssProcessor->expects($this->once())
      ->method('filter')
      ->with($css)
      ->willReturn($css);

    $filename = $this->cssStorage->store($id, $css, $filter);
    $this->assertSame($expected_filename, $filename);
  }

}

if (!defined('FILE_EXISTS_RENAME')) {
  define('FILE_EXISTS_RENAME', 0);
}
if (!defined('FILE_CREATE_DIRECTORY')) {
  define('FILE_CREATE_DIRECTORY', 1);
}
if (!defined('FILE_MODIFY_PERMISSIONS')) {
  define('FILE_MODIFY_PERMISSIONS', 2);
}
